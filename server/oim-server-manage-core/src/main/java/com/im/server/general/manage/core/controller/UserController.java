package com.im.server.general.manage.core.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.im.server.general.common.bean.User;
import com.im.server.general.common.data.system.UserInfo;
import com.im.server.general.common.data.system.UserInfoQuery;
import com.im.server.general.manage.common.annotation.PermissionMapping;
import com.im.server.general.manage.system.service.UserService;
import com.onlyxiahui.common.message.result.ResultMessage;
import com.onlyxiahui.general.annotation.parameter.Define;
import com.onlyxiahui.general.annotation.parameter.RequestParameter;
import com.onlyxiahui.im.message.data.PageData;
import com.onlyxiahui.query.page.DefaultPage;

/**
 * 
 * date 2018-07-19 09:29:16<br>
 * description 系统用户管理
 * @author XiaHui<br>
 * @since
 */
@Controller
@RequestMapping("/manage/core")
public class UserController {
	@Resource
	UserService userService;

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "用户列表", key = "/manage/core/user/list", superKey = "core", type = PermissionMapping.Type.menu)
	@RequestMapping(method = RequestMethod.POST, value = "/user/list")
	public Object list(HttpServletRequest request,
			@Define("userQuery") UserInfoQuery userQuery,
			@Define("page") PageData page) {
		ResultMessage rm = new ResultMessage();
		try {
			DefaultPage defaultPage = new DefaultPage();
			defaultPage.setPageNumber(page.getPageNumber());
			defaultPage.setPageSize(page.getPageSize());
			userQuery.setType(User.type_general);
			List<UserInfo> list = userService.queryUserDataList(userQuery, defaultPage);
			rm.put("list", list);
			rm.put("page", defaultPage);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "编辑用户", key = "/manage/core/user/addOrUpdate", superKey = "/manage/core/user/list")
	@RequestMapping(method = RequestMethod.POST, value = "/user/addOrUpdate")
	public Object addOrUpdate(HttpServletRequest request,
			@Define("user") User user) {
		ResultMessage rm = new ResultMessage();
		try {
			user.setType(User.type_general);
			if (null == user.getId() || "".equals(user.getId())) {
				User u = userService.getUserByAccount(user.getAccount());
				if (u == null) {
					user.setId(null);
					userService.addOrUpdate(user);
				} else {
					rm.addWarning("001", "账号已存在！");
				}
			} else {
				User u = userService.getUserByAccount(user.getAccount());
				if (u == null) {
					userService.addOrUpdate(user);
				} else {
					if (user.getId().equals(u.getId())) {
						userService.addOrUpdate(user);
					}
				}
			}
			rm.put("user", user);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "获取用户", key = "/manage/core/user/get", superKey = "/manage/core/user/list")
	@RequestMapping(method = RequestMethod.POST, value = "/user/get")
	public Object get(HttpServletRequest request,
			@Define("id") String id) {
		ResultMessage rm = new ResultMessage();
		try {
			User user = userService.getById(id);
			rm.put("user", user);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "删除用户", key = "/manage/core/user/delete", superKey = "/manage/core/user/list")
	@RequestMapping(method = RequestMethod.POST, value = "/user/delete")
	public Object delete(HttpServletRequest request,
			@Define("id") String id) {
		ResultMessage rm = new ResultMessage();
		try {
			userService.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "账号是否存在", key = "/manage/core/user/isExist", superKey = "/manage/core/user/list")
	@RequestMapping(method = RequestMethod.POST, value = "/user/isExist")
	public Object isExistAccount(
			HttpServletRequest request,
			@Define("id") String id,
			@Define("account") String account) {
		ResultMessage rm = new ResultMessage();
		boolean isExist = userService.isExistAccount(account, id);
		rm.put("exist", isExist);
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "获取用户详情", key = "/manage/core/user/getInfo", superKey = "/manage/core/user/list")
	@RequestMapping(method = RequestMethod.POST, value = "/user/getInfo")
	public Object loadInfo(
			HttpServletRequest request,
			@Define("id") String id) {
		ResultMessage rm = new ResultMessage();
		try {
			User user = userService.getById(id);
			rm.put("user", user);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "修改密码", key = "/manage/core/user/updatePassword", superKey = "/manage/core/user/list")
	@RequestMapping(method = RequestMethod.POST, value = "/user/updatePassword")
	public Object saveUpdatePassword(ModelMap map, 
			@Define("id") String userId, 
			@Define("password") String password) {
		ResultMessage rm = new ResultMessage();
		try {
			String text = "";
			boolean mark = true;
			if (null == password || "".equals(password)) {
				mark = false;
				text = ("密码不能为空！");
			}
			if (mark) {
				mark = userService.updatePassword(userId, password);
				if (mark) {
					text = ("修改成功！");
				} else {
					text = ("修改失败！");
				}
			}
			if (!mark) {
				rm.addWarning("001", text);
			}
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}
	
	
	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "设为管理员", key = "/manage/core/user/toAdmin", superKey = "/manage/core/user/list")
	@RequestMapping(method = RequestMethod.POST, value = "/user/toAdmin")
	public Object toAdmin(ModelMap map, 
			@Define("id") String userId, 
			@Define("type") String type) {
		ResultMessage rm = new ResultMessage();
		try {
			String text = "";
			boolean mark = true;
			if (mark) {
				mark = userService.updateType(userId, User.type_admin);
				if (mark) {
					text = ("修改成功！");
				} else {
					text = ("修改失败！");
				}
			}
			if (!mark) {
				rm.addWarning("001", text);
			}
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}
}
