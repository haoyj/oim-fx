package com.im.server.general.manage.index.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.im.server.general.common.app.UserContext;
import com.im.server.general.common.bean.system.Menu;
import com.im.server.general.manage.common.annotation.PermissionMapping;
import com.im.server.general.manage.index.service.IndexService;
import com.onlyxiahui.common.message.result.ResultMessage;
import com.onlyxiahui.general.annotation.parameter.Define;
import com.onlyxiahui.general.annotation.parameter.RequestParameter;

/**
 * date 2018-06-04 14:59:44<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@RestController
@RequestMapping("/manage")
public class IndexController {

	@Resource
	IndexService indexService;

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "登录", key = "/manage/index/login", superKey = "index", isIntercept = false)
	@RequestMapping(method = RequestMethod.POST, value = "/index/login")
	public Object login(
			HttpServletRequest request,
			@Define("account") String account,
			@Define("password") String password) {
		ResultMessage rm = indexService.login(account, password);
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "加载菜单", key = "/manage/index/menuList", superKey = "index", isIntercept = false)
	@RequestMapping(method = RequestMethod.POST, value = "/index/menuList")
	public Object menuList(
			HttpServletRequest request,
			UserContext userContext) {
		String userId = userContext.getCurrentUserId();
		String token = userContext.getToken();
		ResultMessage rm = new ResultMessage();
		List<Menu> list = indexService.menuList(userId,token);
		rm.put("list", list);
		return rm;
	}
}
