import boxUtil from '../lib/box.util';

const menuBox = {}

const treeList = [
    {
        icon: 'key',
        name: 'system',
        title: '权限管理',
        children: [
            {
                icon: 'key',
                name: 'manage.system.menu.allList',
                title: '菜单管理'
            },
            {
                icon: 'key',
                name: 'manage.system.role.list',
                title: '角色管理'
            },
            {
                icon: 'key',
                name: 'manage.system.admin.list',
                title: '系统用户管理'
            }
        ]
    },
    {
        icon: 'key',
        name: 'core',
        title: '用户数据',
        children: [
            {
                icon: 'key',
                name: 'manage.core.user.list',
                title: '用户管理'
            },
            {
                icon: 'key',
                name: 'manage.core.group.list',
                title: '群管理'
            }
        ]
    }
]

menuBox.getMenuTreeList = function (list, type) {
    var menuList = [];
    if (2 === type || '2' === type) {
        menuList = treeList;
    } else {
        var tempList = [];
        var allMap = boxUtil.getMap('id', list, '');
        boxUtil.copyTreeList(menuList, treeList, 'name', 'children', allMap);
    }
    return menuList;
};


export default menuBox;
